using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Test_Driven_Development;

namespace BusinessTests
{
    [TestClass]
    public class BusinessTests
    {
        [TestMethod]
        public void EmployeeSuccessfullyAddedToList()
        {
            // Arrange
            Business business = new Business(new List<Employee>(), new List<Job>());

            // Act
            business.AddEmployee(new Employee(10, 10));
            business.AddEmployee(new Employee(10, 10));

            // Assert
            Assert.AreEqual(2, business.Employees.Count);
        }

        [TestMethod]
        public void JobAddedToListSuccessfullyTest()
        {
            // Arrange
            Business business = new Business(new List<Employee>(), new List<Job>());

            // Act
            business.AddJob(new Job(5));
            business.AddJob(new Job(10));

            // Assert
            Assert.AreEqual(2, business.Jobs.Count);
        }

        [TestMethod]
        public void JobCompleteAfterWorkTest()
        {
            // Arrange
            Business business = new Business(new List<Employee>(), new List<Job>());
            business.AddJob(new Job(2));
            business.AddEmployee(new Employee(10, 5));

            // Act
            business.DoWork();

            // Assert
            Assert.IsTrue(business.Jobs[0].JobCompleted);
        }

        [TestMethod]
        public void JobIncompleteWhenOutOfEmployees()
        {
            // Arrange
            Business business = new Business(new List<Employee>(), new List<Job>());
            business.AddJob(new Job(15));
            business.AddEmployee(new Employee(10, 5));
            business.AddEmployee(new Employee(10, 7));

            // Act
            business.DoWork();

            // Assert
            Assert.IsFalse(business.Jobs[0].JobCompleted);
        }

        [TestMethod]
        public void JobIncompleteWhenEmployeeOutOfHours()
        {
            // Arrange
            Business business = new Business(new List<Employee>(), new List<Job>());
            business.AddJob(new Job(15));
            business.AddJob(new Job(10));
            business.AddEmployee(new Employee(10, 19));

            // Act
            business.DoWork();

            // Assert
            Assert.IsFalse(business.Jobs[1].JobCompleted);
        }

        [TestMethod]
        public void PaycheckAddedSuccesfullyAfterWork()
        {
            // Arrange
            Business business = new Business(new List<Employee>(), new List<Job>());
            business.AddJob(new Job(8));
            Employee employee = new Employee(10, 9);
            business.AddEmployee(employee);

            // Act
            business.DoWork();

            // Assert
            Assert.AreEqual(80, employee.Paycheck);
        }

        [TestMethod]
        public void JobCostAddedSuccessfullyAfterWork()
        {
            // Arrange
            Business business = new Business(new List<Employee>(), new List<Job>());
            Job job = new Job(8);
            business.AddJob(job);
            Employee employee = new Employee(10, 9);
            business.AddEmployee(employee);

            // Act
            business.DoWork();

            // Assert
            Assert.AreEqual(120, job.JobCost);
        }
    }
}
