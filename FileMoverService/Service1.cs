﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;

namespace FileMoverService
{
    public partial class Service1 : ServiceBase
    {
        public FileSystemWatcher fileSystemWatcher = new FileSystemWatcher(@"C:\ADN\ADN");

        public Service1()
        {
            InitializeComponent();
            AttachMethods();
        }

        public void AttachMethods()
        {
            fileSystemWatcher.EnableRaisingEvents = true;
            fileSystemWatcher.Created += OnCreate;
            fileSystemWatcher.Changed += OnChange;
            fileSystemWatcher.Renamed += OnRename;
        }

        private static void MoveFile(string sourcePath, string destinationPath)
        {
            if (File.Exists(sourcePath) == true)
            {
                Thread.Sleep(1000);

                if (File.Exists(destinationPath) == true)
                {
                    File.Delete(destinationPath);
                }

                File.Move(sourcePath, destinationPath);
                Console.WriteLine($"The file has been moved from {sourcePath} to {destinationPath}");
            }
        }

        private static void OnChange(object sender, FileSystemEventArgs e)
        {
            EventLog log = new EventLog();
            log.Source = "Application";
            log.WriteEntry("The file was moved successfully!", EventLogEntryType.Information);
        }

        private static void OnCreate(object sender, FileSystemEventArgs e)
        {
            try
            {
                MoveFile(e.FullPath, $@"C:\Users\damia\Dropbox\Semester 4\Adv .NET B\target\{e.Name}");
            }
            catch
            {
                EventLog log = new EventLog();
                log.Source = "Application";
                log.WriteEntry("The file could not be moved.", EventLogEntryType.Error);
            }
        }

        private static void OnRename(object sender, FileSystemEventArgs e)
        {
            try
            {
                MoveFile(e.FullPath, $@"C:\Users\damia\Dropbox\Semester 4\Adv .NET B\target\{e.Name}");
            }
            catch
            {
                EventLog log = new EventLog();
                log.Source = "Application";
                log.WriteEntry("The file could not be moved.", EventLogEntryType.Error);
            }
        }

        protected override void OnStart(string[] args)
        {
            EventLog log = new EventLog();
            log.Source = "Application";
            log.WriteEntry("The service was started.");
        }

        protected override void OnStop()
        {
            EventLog log = new EventLog();
            log.Source = "Application";
            log.WriteEntry("The service was stopped.");
        }
    }
}
