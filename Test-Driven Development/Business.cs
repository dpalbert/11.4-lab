﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test_Driven_Development
{
    /// <summary>
    /// The class which is used to represent a Business.
    /// </summary>
    [Serializable]
    public class Business
    {
        /// <summary>
        /// Initializes a new instance of the Business class.
        /// </summary>
        /// <param name="employees">The business's list of employees.</param>
        /// <param name="jobs">The business's list of jobs.</param>
        public Business(List<Employee> employees, List<Job> jobs)
        {
            this.Employees = employees;
            this.Jobs = jobs;
        }

        private Business()
        {
        }

        /// <summary>
        /// Gets or sets the business's list of employees.
        /// </summary>
        public List<Employee> Employees { get; set; }

        /// <summary>
        /// Gets or sets the business's jobs.
        /// </summary>
        public List<Job> Jobs { get; set; }

        /// <summary>
        /// Adds an employee to the business.
        /// </summary>
        /// <param name="employee">The employee to add.</param>
        public void AddEmployee(Employee employee)
        {
            // Add employee to the employee list.
            this.Employees.Add(employee);
        }

        /// <summary>
        /// Adds a job to the jobs list.
        /// </summary>
        /// <param name="job">The job to be added.</param>
        public void AddJob(Job job)
        {
            // Add job to the job list
            this.Jobs.Add(job);
        }

        /// <summary>
        /// Have employees do their jobs.
        /// </summary>
        public void DoWork()
        {
            // For each job, if job isn't completed, check each employees to see if they have hours and have them DoWork(). If job is completed after work, break from loop.
            foreach (Job j in this.Jobs)
            {
                if (j.JobCompleted != true)
                {
                    foreach (Employee e in this.Employees)
                    {
                        if (e.HoursScheduled > 0)
                        {
                            e.DoWork(j);
                            if (j.JobCompleted == true)
                            {
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
}
