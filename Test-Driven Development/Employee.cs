﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test_Driven_Development
{
    /// <summary>
    /// The class which is used to represent an Employee.
    /// </summary>
    [Serializable]
    public class Employee
    {
        private decimal hourlyWage;

        private int hoursScheduled;

        /// <summary>
        /// Initializes a new instance of the Employee class.
        /// </summary>
        /// <param name="hourlyWage">The employee's hourly wage.</param>
        /// <param name="hoursScheduled">The hours the employee is scheduled to work.</param>
        public Employee(decimal hourlyWage, int hoursScheduled)
        {
            this.HourlyWage = hourlyWage;
            this.HoursScheduled = hoursScheduled;
        }

        private Employee()
        {
        }

        /// <summary>
        /// Gets or sets the employee's paycheck.
        /// </summary>
        public decimal Paycheck { get; set; }

        /// <summary>
        /// Gets or sets the employee's hours scheduled.
        /// </summary>
        public int HoursScheduled
        {
            get
            {
                return this.hoursScheduled;
            }

            set
            {
                this.hoursScheduled = value;
            }
        }

        /// <summary>
        /// Gets or sets the employee's hourly wage.
        /// </summary>
        public decimal HourlyWage
        {
            get
            {
                return this.hourlyWage;
            }

            set
            {
                this.hourlyWage = value;
            }
        }

        /// <summary>
        /// Has the employee do a job.
        /// </summary>
        /// <param name="work">The job to be done.</param>
        public void DoWork(Job work)
        {
            // If the hoursScheduled are less than the time remaining on the job, reduce the hours remaining on the job by the hours scheduled, and set hours scheduled to zero.
            // Else reduce hours scheduled by time remaining, and set time investment for job to 0.
            if (this.HoursScheduled < work.TimeInvestmentRemaining)
            {
                work.TimeInvestmentRemaining -= this.HoursScheduled;
                this.Paycheck = this.HoursScheduled * this.HourlyWage;
                this.HoursScheduled = 0;
            }
            else
            {
                this.HoursScheduled -= work.TimeInvestmentRemaining;
                this.Paycheck = work.TimeInvestmentRemaining * this.HourlyWage;
                work.JobCost = this.Paycheck * 1.5M;
                work.TimeInvestmentRemaining = 0;
            }
        }
    }
}
