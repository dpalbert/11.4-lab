﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test_Driven_Development
{
    /// <summary>
    /// The class which is used to represent a job.
    /// </summary>
    [Serializable]
    public class Job
    {
        /// <summary>
        /// Initializes a new instance of the job class.
        /// </summary>
        /// <param name="timeInvestment">The time needed to complete the job.</param>
        public Job(int timeInvestment)
        {
            this.TimeInvestmentRemaining = timeInvestment;
        }

        private Job()
        {
        }

        /// <summary>
        /// Gets or sets the time investment remaining.
        /// </summary>
        public int TimeInvestmentRemaining { get; set; }

        /// <summary>
        /// Gets or sets the job's cost.
        /// </summary>
        public decimal JobCost { get; set; }

        /// <summary>
        /// Gets a value indicating whether the job has been completed.
        /// </summary>
        public bool JobCompleted => this.TimeInvestmentRemaining == 0;
    }
}
