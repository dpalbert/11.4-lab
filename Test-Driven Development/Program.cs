﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;

namespace Test_Driven_Development
{
    /// <summary>
    /// The class which is used to represent a program.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// The program's main method.
        /// </summary>
        /// <param name="args">The method's arguments.</param>
        private static void Main(string[] args)
        {
            Business business = new Business(new List<Employee>(), new List<Job>());
            business.AddEmployee(new Employee(10, 10));
            business.AddEmployee(new Employee(10, 10));
            business.AddJob(new Job(15));
            business.DoWork();

            SerializeBusiness(business);
        }

        /// <summary>
        /// Serializes businesses.
        /// </summary>
        /// <param name="business">The business to serialize.</param>
        private static void SerializeBusiness(Business business)
        {
            // Create a binary formatter
            XmlSerializer formatter = new XmlSerializer(typeof(Business));

            // Create a file using the passed-in file name
            // Use a using statement to automatically clean up object references
            // and close the file handle when the serialization process is complete
            using (Stream stream = File.Create(@"C:\ADN\ADN"))
            {
                // Serialize (save) the current instance of the business
                formatter.Serialize(stream, business);
            }
        }
    }
}
